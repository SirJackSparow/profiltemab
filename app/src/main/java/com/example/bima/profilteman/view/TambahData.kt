package com.example.bima.profilteman.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.bima.profilteman.R
import com.example.bima.profilteman.model.ModelData
import com.example.bima.profilteman.model.ResponseData
import com.example.bima.profilteman.model.api.GetandPostData
import com.example.bima.profilteman.model.api.ServicesApi
import com.example.bima.profilteman.presenter.Presenter
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Retrofit

class TambahData: AppCompatActivity(),Views {

    private lateinit var retrofit: GetandPostData
    private lateinit var addData : Call<ResponseData>
    private lateinit var presenter : Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var nama = namas.text
        var usia = usia.text
        var domisili = domisili.text
        save.setOnClickListener {
            retrofit = ServicesApi.retrofit().create(GetandPostData::class.java)
            addData = retrofit.tambahData(nama.toString(),usia.toString(),domisili.toString())
            presenter = Presenter(this,retrofit)
            presenter.tambahData(addData)

        }
    }

    override fun getData(data: List<ModelData>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun kode(kode: String) {

        if (kode == "1"){
            ctx.startActivity<MainActivity>()
            finish()
        }else {
            toast("gagal")
        }
    }

}