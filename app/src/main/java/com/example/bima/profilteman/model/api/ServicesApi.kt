package com.example.bima.profilteman.model.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ServicesApi {
    companion object {
        fun retrofit () : Retrofit{
            var retrofit = Retrofit.Builder()
                        .baseUrl( "http://10.0.2.2/baru/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()

            return retrofit
        }

    }
}