package com.example.bima.profilteman.view

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.example.bima.profilteman.R
import com.example.bima.profilteman.model.ModelData
import com.example.bima.profilteman.model.ResponseData
import com.example.bima.profilteman.model.api.GetandPostData
import com.example.bima.profilteman.model.api.ServicesApi
import com.example.bima.profilteman.presenter.Presenter
import kotlinx.android.synthetic.main.detail.*
import kotlinx.android.synthetic.main.dialog.*
import kotlinx.android.synthetic.main.dialog.view.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Retrofit

class Detail : AppCompatActivity(),Views {


    lateinit var response : Call<ResponseData>
    lateinit var presenter: Presenter
    lateinit var retrofit: GetandPostData

    var id: String? = null
    var namess: String?=null
    var usiass: String? = null
    var domisiliss: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail)
        val intent: Intent = intent
        id = intent.getStringExtra("id")
        namess = intent.getStringExtra("name")
        usiass = intent.getStringExtra("usia")
        domisiliss = intent.getStringExtra("domisili")
        nama.text = namess
        usia.text = usiass
        domisili.text = domisiliss

        toast("${namess.toString()}")

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
       when(item?.itemId){
           R.id.edit -> {
               //deletess.visibility = View.GONE
               val dialogBuilder = AlertDialog.Builder(this)
               val view = layoutInflater.inflate(R.layout.dialog,null)

               dialogBuilder.setView(view)
               dialogBuilder.setTitle(" Edit")

               dialogBuilder.setPositiveButton("Yes") {
                       _: DialogInterface, _:Int ->

                   val nama = view.names.text
                   val usia = view.usias.text
                   val domisili = view.domisilis.text

                   retrofit = ServicesApi.retrofit().create(GetandPostData::class.java)
                   response = retrofit.updateData(id.toString(),nama.toString(),usia.toString(),domisili.toString())
                   presenter = Presenter(this,retrofit)
                   presenter.updateData(response)

               }
               dialogBuilder.setNegativeButton("No"){
                       _: DialogInterface, _:Int ->

               }
               dialogBuilder.show()

           }
           R.id.deletes -> {
               //editess.visibility = View.GONE
               val dialogBuilder = AlertDialog.Builder(this)

               dialogBuilder.setTitle("Apakah anda yakin")

               dialogBuilder.setPositiveButton("Yes"){
                       _: DialogInterface, _:Int ->
                   retrofit = ServicesApi.retrofit().create(GetandPostData::class.java)
                   response = retrofit.deleteData(this.id!!)
                   presenter = Presenter(this,retrofit)
                   presenter.deleteData(response)
               }
               dialogBuilder.setNegativeButton("No"){
                       _: DialogInterface, _:Int ->

               }

               dialogBuilder.show()
           }
           else -> {
               ctx.startActivity<TambahData>()
           }
       }
        return true
    }

    override fun getData(data: List<ModelData>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun kode(kode: String) {
        if (kode == "1"){
            ctx.startActivity<MainActivity>()
            finish()
        }else {
            toast("gagal")
        }
    }


}