package com.example.bima.profilteman.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.example.bima.profilteman.R
import com.example.bima.profilteman.adapter.Adapter
import com.example.bima.profilteman.model.ModelData
import com.example.bima.profilteman.model.ResponseData
import com.example.bima.profilteman.model.api.GetandPostData
import com.example.bima.profilteman.model.api.ServicesApi
import com.example.bima.profilteman.presenter.Presenter
import kotlinx.android.synthetic.main.view_list.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.startActivity
import retrofit2.Call

class MainActivity : AppCompatActivity(),Views {

    private lateinit var adapter: Adapter
    private lateinit var retrofits: GetandPostData
    private lateinit var getData: Call<ResponseData>
    private var datas: MutableList<ModelData> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_list)

        fab_add.setOnClickListener {
            ctx.startActivity<TambahData>()
        }

        retrofits = ServicesApi.retrofit().create(GetandPostData::class.java)
        getData = retrofits.listData()

        var predenter = Presenter(this,retrofits)

        predenter.getData(getData)
        adapter = Adapter(datas){
            var intent = Intent(this@MainActivity,Detail::class.java)
            intent.putExtra("id", it.id)
            intent.putExtra("name",it.nama)
            intent.putExtra("usia",it.usia)
            intent.putExtra("domisili",it.domisili)
            startActivity(intent)
        }
        item_recyclers.adapter = adapter
        item_recyclers.layoutManager = LinearLayoutManager(applicationContext)
    }


    override fun getData(data: List<ModelData>) {
        datas.clear()
        datas.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun kode(kode: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
