package com.example.bima.profilteman.view

import com.example.bima.profilteman.model.ModelData
import com.example.bima.profilteman.model.ResponseData
import retrofit2.Call

interface Views {
    fun getData(data: List<ModelData>)
    fun kode(kode: String)
}