package com.example.bima.profilteman.presenter

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.example.bima.profilteman.model.ResponseData
import com.example.bima.profilteman.model.api.GetandPostData
import com.example.bima.profilteman.model.api.ServicesApi
import com.example.bima.profilteman.view.MainActivity
import com.example.bima.profilteman.view.Views
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Presenter (private  val view: Views,private var retrofit: GetandPostData){

    fun getData(getData:Call<ResponseData>){
        retrofit = ServicesApi.retrofit().create(GetandPostData::class.java)
        getData.enqueue(object : Callback<ResponseData>{
            override fun onFailure(call: Call<ResponseData>, t: Throwable) {
                Log.d(TAG,"Gagal")
            }
            override fun onResponse(call: Call<ResponseData>, response: Response<ResponseData>) {
               var data = response.body()?.result!!
               view.getData(data)
            }
        })

    }
    fun tambahData(tambahData:Call<ResponseData>){
        retrofit = ServicesApi.retrofit().create(GetandPostData::class.java)
        tambahData.enqueue(object : Callback<ResponseData>{
            override fun onFailure(call: Call<ResponseData>, t: Throwable) {
                Log.d(TAG,"Gagal")
            }
            override fun onResponse(call: Call<ResponseData>, response: Response<ResponseData>) {
               var kode = response.body()!!.kode
                view.kode(kode)
            }
        })


    }
    fun deleteData(delete: Call<ResponseData>){
       delete.enqueue(object : Callback<ResponseData>{
           override fun onFailure(call: Call<ResponseData>, t: Throwable) {
               TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
           }

           override fun onResponse(call: Call<ResponseData>, response: Response<ResponseData>) {
              var kode = response.body()!!.kode
               view.kode(kode)
           }

       })
    }
    fun updateData(update: Call<ResponseData>){
        update.enqueue(object : Callback<ResponseData>{
            override fun onFailure(call: Call<ResponseData>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(call: Call<ResponseData>, response: Response<ResponseData>) {
                var kode = response.body()!!.kode
                view.kode(kode)
            }

        })
    }

}