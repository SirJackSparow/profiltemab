package com.example.bima.profilteman.model.api

import com.example.bima.profilteman.model.ResponseData
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface GetandPostData {

    //Get
    @GET("read.php")
    fun listData():Call<ResponseData>

    //POST
    @FormUrlEncoded
    @POST("insert.php")
    fun tambahData(@Field("nama")  nama: String,
                   @Field("usia") usia: String,
                   @Field("domisili") domisili: String) : Call<ResponseData>
    @FormUrlEncoded
    @POST("delete.php")
    fun deleteData(@Field("id") id:String):Call<ResponseData>

    @FormUrlEncoded
    @POST("update.php")
    fun updateData(@Field("id") id:String,
                   @Field("nama")  nama: String,
                   @Field("usia") usia: String,
                   @Field("domisili") domisili: String):Call<ResponseData>


}