package com.example.bima.profilteman.view

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.bima.profilteman.R.id.*
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class UIAdapter : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui){
        cardView {
            lparams(matchParent, wrapContent){
                margin = dip(5)
            }
                elevation = dip(4).toFloat()

            linearLayout {
                lparams(width= matchParent, height = wrapContent)
                padding = dip(15)
                orientation = LinearLayout.VERTICAL

                linearLayout {
                    lparams(matchParent, wrapContent)
                    orientation = LinearLayout.HORIZONTAL

                    textView {
                       text = "Nama :"
                    }

                    textView {
                        id = nama_teman
                        textSize = 16f
                    }.lparams{
                        marginStart = dip(5)
                    }
                }

                linearLayout {
                    lparams(matchParent, wrapContent)
                    orientation = LinearLayout.HORIZONTAL

                    textView {
                        text = "Usia :"
                    }

                    textView {
                        id = usia_teman
                        textSize = 16f
                    }.lparams{
                        marginStart = dip(5)
                    }

                }

                linearLayout {
                    lparams(matchParent, wrapContent)
                    orientation = LinearLayout.HORIZONTAL

                    textView {
                        text = "Domisili :"
                    }

                    textView {
                        id = domisili_teman
                        textSize = 16f
                    }
                }
           }

        }
    }

}