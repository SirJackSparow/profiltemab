package com.example.bima.profilteman.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.bima.profilteman.R

import com.example.bima.profilteman.model.ModelData
import com.example.bima.profilteman.view.UIAdapter
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find

class Adapter(private val dataList:List<ModelData>,private val listener: (ModelData)-> Unit)
    : RecyclerView.Adapter<MyHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MyHolder {
        return MyHolder(UIAdapter().createView(AnkoContext.create(parent.context, parent)))
     }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(p0: MyHolder, p1: Int) {
        p0.bindItem(dataList[p1],listener)
    }
}

class MyHolder(view: View) : RecyclerView.ViewHolder(view){

    var nama_teman: TextView = view.find(R.id.nama_teman)
    var usia_teman : TextView = view.find(R.id.usia_teman)
    var domisili_teman : TextView = view.find(R.id.domisili_teman)

    fun bindItem(datas: ModelData, listener: (ModelData) -> Unit){
        nama_teman.text = datas.nama
        usia_teman.text = datas.usia
        domisili_teman.text = datas.domisili

        itemView.setOnClickListener {
            listener(datas)
        }
    }
}